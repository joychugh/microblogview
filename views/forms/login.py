__author__ = 'Joy'

from flask.ext.wtf import Form
from wtforms import TextField
from wtforms.validators import Required


class LoginForm(Form):
    """
    The Login Form
    """
    content = TextField('content')