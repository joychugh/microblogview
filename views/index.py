import requests
from flask import render_template, Blueprint


index_page = Blueprint('index', __name__,
                       template_folder='templates')


@index_page.route('/', defaults={'page': 'index'})
def get(page):
    url = "http://localhost:5000/blog/posts"
    r = requests.get(url)
    posts = r.json()
    return render_template('%s.html' % page,
                           user='Joy',
                           posts=posts)