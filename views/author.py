__author__ = 'Joy'
import requests
from pytz import timezone
from flask import render_template, Blueprint, flash, redirect
from dateutil.parser import parse
from forms import LoginForm
import simplejson as json

author_page = Blueprint('author', __name__, template_folder='templates')
eastern = timezone('US/Eastern')
dateformat = "%a, %d %b %Y %H:%M:%S %z"


@author_page.route('/author/<int:id>', methods=['GET', 'POST'])
def author(id):
    # TODO: Encapsulate the request logic.
    # TODO: Encapsulate the request logic.
    url = "http://localhost:5000/blog/user/{id}".format(id=id)
    r = requests.get(url)
    author = r.json()
    # TODO: Encapsulate this logic.
    for post in author['posts']:
        post['timestamp'] = parse(post['timestamp']).astimezone(eastern)
    form = LoginForm(csrf_enabled=False)
    if form.validate_on_submit():
        payload = {"body": str(form.content.data),
                   "user_id": id}
        headers = {'content-type': 'application/json'}
        requests.post("http://localhost:5000/blog/post", json.dumps(payload), headers=headers)
        return redirect('/')
    return render_template('author.html', user='Joy', author=author, form=form)