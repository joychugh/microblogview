from flask import Flask

app = Flask(__name__)

from views import index, author
app.register_blueprint(index)
app.register_blueprint(author)